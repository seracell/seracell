# coding=utf-8
import time
from datetime import datetime
from neopixel import *
import argparse
from omxplayer.player import OMXPlayer
from pathlib import Path
from time import sleep
import requests
import json
from gtts import gTTS
import os
import sys

reload(sys)
sys.setdefaultencoding('utf-8')

LED_COUNT      = 30
LED_PIN        = 21
LED_FREQ_HZ    = 800000
LED_DMA        = 10
LED_BRIGHTNESS = 255
LED_INVERT     = False
LED_CHANNEL    = 0


THUNDER = Path("/boot/thunder.mp3")
JNEWTON = Path("/boot/JuiceNewton.mp3")
RAIN    = Path("/boot/Rain.mp3")
TORNADO = Path("/boot/Tornado.mp3")
WIND0   = Path("/boot/Wind00.mp3")
WIND1   = Path("/boot/wind0.mp3")
WIND2   = Path("/boot/Wind.mp3")
SPEECH  = Path("response.mp3")

strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
strip.begin()


def getJsonParse():
   user_lig = user_pol = user_pre = isLightning = isPolygon = isPrecipitation = severity = direction = arrival_time = ''

   response = requests.get('http://35.210.8.112:8080/List?user=' + getSerial())
   data = response.json()

   for p in data['lightning']:
                   user_lig = p['user']
                   isLightning = str(p['isLightning'])

   for p in data['polygon']:
                   user_pol = p['user']
                   severity = p['severity']
                   isPolygon = str(p['isPolygon'])

   for p in data['precipitation']:
                   user_pre =  p['user']
                   direction = p['direction']
                   arrival_time = str(p['arrival_time'])
                   isPrecipitation = str(p['isPrecipitation'])

   return isLightning, isPolygon, isPrecipitation, severity


def getLocation():
   latitude = longitude = ''
   response = requests.get('http://35.210.8.112:8080/Location?user=' + getSerial())
   data = response.json()

   for p in data:
                   latitude = p['lat']
                   longitude = p['lng']

   return latitude, longitude


def getJsonParseDaily():
   time_day = ''
   icon = temp_max = temp_min = prec = prec_prob = hum = windspeed = winddirection = 0

   latitude, longitude = getLocation()
   response = requests.get("http://world.tarla.in/v3/weather/daily?lat=" + latitude  + "&lng=" + longitude, headers={'apikey':'sfSKWne5af25asna3'})
   data = response.json()

   rpi_date = datetime.now().strftime("%Y-%m-%d")

   for p in data['daily']:
           if rpi_date == p['time']:
                time_day = p['time']
                icon = p['icon']
                temp_max = p['temperature_max']
                temp_min = p['temperature_min']
                prec = p['precipitation']
                prec_prob = p['precipitation_probability']
                hum = p['humidity']
                windspeed = p['windspeed']
                winddirection = p['winddirection']

   tts = gTTS(text='Merhaba, bugün günlerden '+ rpi_date +' en düşük sıcaklık '+ str(round(temp_min,1)) +' derece, en yüksek sıcaklık '+ str(round(temp_max,1)) +' derece, yağış olasılığı, yüzde '+
              str(prec_prob) +' , havadaki nem oranı, yüzde '+ str(hum) +', rüzgar hızı saatte '+ str(round(windspeed,1)) +' kilometre'+ 'Mutlu günler   ', lang='tr')
   tts.save("response.mp3")
   player = OMXPlayer(SPEECH)
   sleep(40)
   return icon

   
def getSerial():
  cpuserial = "0000000000000000"
  try:
    f = open('/proc/cpuinfo','r')
    for line in f:
      if line[0:6]=='Serial':
        cpuserial = line[10:26]
    f.close()
  except:
    cpuserial = "ERROR000000000"

  return cpuserial


def Leds(strip, color, wait_ms = 500):
        for i in range(strip.numPixels()):
                strip.setPixelColor(i, color)
                strip.show()
                time.sleep(wait_ms/1000.0)


def Led(pin ,strip, color, wait_ms = 5):

        strip.setPixelColor(pin,color)
        strip.show()
        time.sleep(wait_ms/1000.0)


def Green(pin):
        Led(pin,strip,Color(255, 0, 0))

def Red(pin):
        Led(pin,strip,Color(0, 255, 0))

def Blue(pin):
        Led(pin,strip,Color(0, 0, 255))

def Yellow(pin):
        Led(pin,strip,Color(255, 255, 0))

def White(pin):
        Led(pin,strip,Color(255, 255, 255))

def Purple(pin):
        Led(pin,strip,Color(0, 128, 128))

def Orange(pin):
        Led(pin,strip,Color(140,255,0))

def Led_Off(pin):
        Led(pin,strip,Color(0, 0, 0))


def Thunder():
        i = 0
        while i < 3:
                White(0)
                White(1)
                White(2)
                White(3)
                White(4)
                Led_Off(0)
                Led_Off(1)
                Led_Off(2)
                Led_Off(3)
                Led_Off(4)
                i += 1

        j = 0
        while j < 3:
                White(20)
                White(21)
                White(22)
                White(23)
                White(24)
                Led_Off(20)
                Led_Off(21)
                Led_Off(22)
                Led_Off(23)
                Led_Off(24)
                j += 1

        k = 0
        while k < 3:
                White(10)
                White(11)
                White(12)
                White(13)
                White(14)
                Led_Off(10)
                Led_Off(11)
                Led_Off(12)
                Led_Off(13)
                Led_Off(14)
                k += 1
        k = 0
        while k < 3:
                White(29)
                White(28)
                White(27)
                White(26)
                White(25)
                Led_Off(29)
                Led_Off(28)
                Led_Off(27)
                Led_Off(26)
                Led_Off(25)
                k += 1

#-------------------(Storm0)------------------
def LedsOn0():
        i = 0
        while i < 30:
                Green(i)
                i += 1

def invLedsOn0():
        i = 29
        while i > -1:
                Green(i)
                i -= 1

def Poligon0():
        i = 0
        while i < 30:
                Green(i)
                sleep(100/1000.0)
                i += 1

def invPoligon0():
        i = 29
        while i > -1:
                Green(i)
                sleep(100/1000.0)
                i -= 1

#----------------------------------------
#--------------(Storm1)------------------
def LedsOn1():
        i = 0
        while i < 30:
                Orange(i)
                i += 1

def invLedsOn1():
        i = 29
        while i > -1:
                Orange(i)
                i -= 1

def Poligon1():
        i = 0
        while i < 30:
                Orange(i)
                sleep(100/1000.0)
                i += 1

def invPoligon1():
        i = 29
        while i > -1:
                Orange(i)
                sleep(100/1000.0)
                i -= 1
#-------------------------------------------

#-----------------(Strom2)------------------

def LedsOn2():
        i = 0
        while i < 30:
                Purple(i)
                i += 1

def invLedsOn2():
        i = 29
        while i > -1:
                Purple(i)
                i -= 1

def Poligon2():
        i = 0
        while i < 30:
                Purple(i)
                sleep(100/1000.0)
                i += 1

def invPoligon2():
        i = 29
        while i > -1:
                Purple(i)
                sleep(100/1000.0)
                i -= 1
#--------------------------------------------

def Storm0():
        Poligon0()
        invLedsOff()
        invPoligon0()
        LedsOff()
        LedsOn0()
        invLedsOff()
        invLedsOn0()
        sleep(60)
        LedsOff()

def Storm1():
        Poligon1()
        invLedsOff()
        invPoligon1()
        LedsOff()
        LedsOn1()
        invLedsOff()
        invLedsOn1()
        sleep(60)
        LedsOff()

def Storm2():
        Poligon2()
        invLedsOff()
        invPoligon2()
        LedsOff()
        LedsOn2()
        invLedsOff()
        invLedsOn2()
        sleep(60)
        LedsOff()

def LedsOff():
        i = 0
        while i < 30:
                Led_Off(i)
                i += 1
def invLedsOff():
        i = 29
        while i > -1:
                Led_Off(i)
                i -= 1
#-----------------------SOUNDS--------------------------

def SoundCloudy():
        player = OMXPlayer(JNEWTON)

def SoundThunder():
        player = OMXPlayer(THUNDER)

def SoundRain():
        player = OMXPlayer(RAIN)

def SoundTornado():
        player = OMXPlayer(TORNADO)

def SoundWind0():
        player = OMXPlayer(WIND0)

def SoundWind1():
        player = OMXPlayer(WIND1)

def SoundWind2():
        player = OMXPlayer(WIND2)


#-----------------------WEATHER--------------------------
def Snowy():
        i = 0
        while i < 30:
                Red(i)
                i += 1

def Rainy():
        i = 0
        while i < 30:
                Green(i)
                i += 1

def Foggy():
        i = 0
        while i < 30:
                Yellow(i)
                i += 1

def Cloudy():
        i = 0
        while i < 30:
                Blue(i)
                i += 1

#---------------------------------------------------------

if __name__ == '__main__':

   status = False


   while True:

     now = datetime.now()


     on_min = now.replace(hour=11, minute=42, second=0, microsecond=0)
     on_max = now.replace(hour=21, minute=47, second=0, microsecond=0)

     off_min = now.replace(hour=21, minute=52, second=0, microsecond=0)
     off_max = now.replace(hour=21, minute=57, second=0, microsecond=0)




     if on_min<now and on_max>now and not status:
        icon = getJsonParseDaily()
        status = True
        sleep(10)


        if icon in [6,7,8,11,12,14,16]:
              #-----Rainy--------
              SoundRain()
              Rainy()
              #------------------
        
		if icon in [9,10,13,15,17]:
              #-----Snowy--------
              Snowy()
              #------------------

        if icon in [4,5]:
              #-----Foggy--------
              Foggy()
              #------------------

        if icon>0 or icon<4:
              #-----Cloudy-------
              SoundCloudy()
              Cloudy()
              #------------------

     if not status:
              isLightning, isPolygon, isPrecipitation, severity = getJsonParse()

              if isLightning == 'True':

                 #-----Thunder------
                 SoundThunder()
                 sleep(900/1000.0)
                 Thunder()
                 sleep(5650/1000.0)
                 Thunder()
                 sleep(8600/1000.0)
                 Thunder()
                 #------------------

              if isPolygon == 'True':

                      if severity == 'Low':

                         #-----Storm0------- !!!!!
                         SoundWind0()
                         Storm0()
                         #------------------

                      if severity == 'Medium':

                         #-----Storm1------- !!!!!
                         SoundWind2()
                         Storm1()
                         #------------------

                      if severity == 'High':

                         #-----Storm2------- !!!!!
                         SoundTornado()
                         Storm2()
                         #------------------

			 if isPrecipitation == 'True':
                 #-----Rainy--------
                 SoundRain()
                 Rainy()
                 sleep(60)
                 LedsOff()
                 #------------------

     if off_min<now and off_max>now:
        LedsOff()
        status = False

     sleep(120)

